<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ShopList as Shops;
use App\Models\PurchaseCatalog as Purchase;
use Auth;
class PurchaseCatalog extends Model{
    //use HasFactory;
        public function scopeCurrentUser($query){
            if(Auth::user()->hasRole('admin')){
                return $query;
            }else if(Auth::user()->hasRole('shop_owners')){
                $authC = Shops::where('shop_admin', Auth::user()->id)->get('id');
                foreach($authC as $id){
                    return $query->where(['shop_id'=>$id['id']]);
                }
            }else{
                return false;
            }
        }
        public function save(array $options = []){
            if (!$this->shop_id && Auth::user()) {
                $s = Shops::where('shop_admin', Auth::user()->id)->get('id');
                foreach($s as $ss){
                    $this->shop_id = $ss['id'];
                }
                
            }

            return parent::save();
        }
    
}
